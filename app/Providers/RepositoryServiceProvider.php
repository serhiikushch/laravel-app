<?php
/**
 * Created by PhpStorm.
 * User: sk
 * Date: 24.11.15
 * Time: 19:28
 */

namespace App\Providers;


use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->bind('App\\Repositories\\Contracts\\IFriendshipRepository', 'App\\Repositories\\FriendshipRepository');
    }
}