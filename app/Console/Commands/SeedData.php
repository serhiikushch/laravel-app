<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Redis;

class SeedData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seed:friends {users_num}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $redis = Redis::connection();
        $redis->eval("for _,k in ipairs(redis.call('keys', ARGV[1])) do redis.call('del', k) end", 0, "user:*");

        $num = (int)$this->argument('users_num');
        $num = $num? : 10;
        $max_friends = array('id' => '', 'num' => 0);
        for ($i=1; $i<=$num; $i++) {
            $redis->hMset("user:$i", array('id' => $i, 'first_name' => "user$i first name", 'last_name' => "user$i last name"));
            $friends_num = rand(0, 5);
            for ($j=0; $j<$friends_num; $j++) {
                $friend_id = rand(1,$num);
                if ($friend_id == $i) continue;
                $redis->sadd("user:$i:friends", $friend_id);
                $redis->sadd("user:$friend_id:friends", $i);
            }
        }
        $this->info($num ." users created");
    }

}
