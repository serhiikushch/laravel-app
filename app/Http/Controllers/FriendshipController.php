<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\IFriendshipRepository;
use Illuminate\Http\Request;

use App\Http\Requests;


class FriendshipController extends Controller
{

    /**
     * @var IFriendshipRepository
     */
    protected $friendshipRepository;

    public function __construct(IFriendshipRepository $friendshipRepository)
    {
        $this->friendshipRepository = $friendshipRepository;
    }


    /**
     * Display user friends.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $level = $request->get('level', 1);
        $response = $this->friendshipRepository->getFriendship($id, $level);
        $result = $response ? $response : array();
        return response($result, $response? 200 : 404, array('Content-type' => 'application/json'));
    }

    /**
     * Display friendship requests
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function showRequests($id)
    {
        $response = $this->friendshipRepository->getFriendshipRequests($id);
        return response(array('result' => $response), 200, array('Content-type' => 'application/json'));
    }

    /**
     * Creates or accepts friendship
     *
     * @return \Illuminate\Http\Response
     */
    public function createFriendship(Request $request)
    {
        $userId = (int)$request->get('user_id');
        $friendId = (int)$request->get('friend_id');
        $response = $this->friendshipRepository->createOrAcceptFriendship($userId, $friendId);
        return response(null, $response? 200 : 400, array('Content-type' => 'application/json'));
    }

    /**
     * Remove or reject friendship request.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteFriendship(Request $request)
    {
        $userId = (int)$request->get('user_id');
        $friendId = (int)$request->get('friend_id');
        $response = $this->friendshipRepository->deleteOrRejectFriendship($userId, $friendId);
        return response(null, $response? 200 : 400, array('Content-type' => 'application/json'));
    }
}
