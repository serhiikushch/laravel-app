<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/friendship/{id}', 'FriendshipController@show');
Route::get('/friendship/{id}/requests', 'FriendshipController@showRequests');
Route::post('/friendship/create', 'FriendshipController@createFriendship');
Route::post('/friendship/delete', 'FriendshipController@deleteFriendship');
