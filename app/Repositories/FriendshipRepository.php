<?php
/**
 * Created by PhpStorm.
 * User: sk
 * Date: 24.11.15
 * Time: 18:46
 */

namespace App\Repositories;


use App\Repositories\Contracts\IFriendshipRepository;
use Redis;


class FriendshipRepository implements  IFriendshipRepository
{

    const FRIENDSHIP_SCRIPT = "
        local level = 1
        local deep = tonumber(ARGV[1])

        local level_result = {KEYS[1]}
        local seen = {[KEYS[1]] = true}
        local result = {}
        local friends = {}
        while level <= deep do
            for key, value in pairs(level_result) do
                friends = redis.call('smembers', value .. ':friends');
                for index, user_id in pairs(friends) do
                    if seen['user:' .. user_id] ~= true then
                        result[#result+1] = user_id
                        friends[index] = 'user:' .. user_id
                        seen['user:' .. user_id] = true
                    end
                end
            end
            if #friends > 0 then
                level_result = friends
                level = level + 1
            else break end
        end

        return cjson.encode(result)


    ";


    /**
     * @var \Redis
     */
    protected $redis;

    public function __construct()
    {
        $this->redis = Redis::connection();
    }


    public function getFriendship($userId, $level = 1)
    {
        $friendship = json_decode($this->redis->eval(self::FRIENDSHIP_SCRIPT, 1, "user:$userId", $level));
        return is_array($friendship) ? $this->getFriendById($friendship) : array();
    }

    public function getFriendshipRequests($userId)
    {
        $requests = $this->redis->sMembers("user:$userId:requests");
        return is_array($requests) && $requests ? $this->getFriendById($requests) : array();
    }


    public function createOrAcceptFriendship($userId, $friendId)
    {
        $user = $this->getFriendById($userId);
        $friend = $this->getFriendById($friendId);
        if ($user & $friend) {
            if ($this->redis->sIsMember("user:$userId:requests", $friendId)) {
                $this->redis->transaction(function($tx) use ($userId, $friendId){
                    $tx->sAdd("user:$userId:friends", $friendId);
                    $tx->sAdd("user:$friendId:friends", $userId);
                    $tx->sRem("user:$userId:requests", $friendId);
                });

                return $this->isFriendship($userId, $friendId);
            } else {
                return $this->redis->sAdd("user:$friendId:requests", $userId);
            }
        }

        return false;
    }

    public function deleteOrRejectFriendship($userId, $friendId)
    {
        $user = $this->getFriendById($userId);
        $friend = $this->getFriendById($friendId);
        if ($user & $friend) {
            if ($this->isFriendship($userId, $friendId)) {
                $this->redis->transaction(function($tx) use ($userId, $friendId){
                    $tx->sRem("user:$userId:friends", $friendId);
                    $tx->sRem("user:$friendId:friends", $userId);
                });
            } else {
                $this->redis->sRem("user:$userId:requests", $friendId);
            }
        }

        return !$this->isFriendship($userId, $friendId);

    }


    public function getFriendById($userId)
    {
        if (is_array($userId)) {
            $response = $this->redis->pipeline(function($pipe) use ($userId) {
                foreach ($userId as $id) {
                    $pipe->hgetall("user:$id");
                }
            });
        } else {
            $response = $this->redis->hgetall("user:$userId");
        }

        return $response;
    }

    private function isFriendship($userId, $friendId)
    {
        return !!$this->redis->sIsMember("user:$userId:friends", $friendId) && !!$this->redis->sIsMember("user:$friendId:friends", $userId);
    }

}
