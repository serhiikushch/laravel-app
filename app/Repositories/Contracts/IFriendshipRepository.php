<?php
/**
 * Created by PhpStorm.
 * User: sk
 * Date: 24.11.15
 * Time: 18:40
 */

namespace App\Repositories\Contracts;


interface IFriendshipRepository {

    public function getFriendById($id);
    public function getFriendship($userId, $level = 1);
    public function getFriendshipRequests($userId);
    public function createOrAcceptFriendship($userId, $friendId);
    public function deleteOrRejectFriendship($userId, $friendId);
}