<?php

class FriendshipRepositoryTest extends TestCase
{


    public function testGetFriendById()
    {
        $this->seedData();
        $repository = new App\Repositories\FriendshipRepository();
        $this->assertEquals(array(
            'id' => 1,
            'first_name' => "user1 first name",
            'last_name' => "user1 last name"),
            $repository->getFriendById(1)
        );
    }

    public function testGetFriendship()
    {
        $this->seedData();
        $repository = new App\Repositories\FriendshipRepository();
        $this->assertEquals(array(array(
            'id' => 2,
            'first_name' => "user2 first name",
            'last_name' => "user2 last name")),
            $repository->getFriendship(1)
        );
    }

    public function testGetFriendshipLevel()
    {
        $this->seedData();
        $redis = Redis::connection();
        $repository = new App\Repositories\FriendshipRepository();
        $redis->sadd("user:2:friends", 3);
        $redis->sadd("user:3:friends", 2);

        $redis->sadd("user:3:friends", 5);
        $redis->sadd("user:5:friends", 3);

        $expectedResponse = array(array(
            'id' => 2,
            'first_name' => "user2 first name",
            'last_name' => "user2 last name"));
        $this->assertEquals($expectedResponse,
            $repository->getFriendship(1)
        );
        //level 2
        $expectedResponse[] = array('id' => 3,
            'first_name' => "user3 first name",
            'last_name' => "user3 last name"
        );

        $this->assertEquals($expectedResponse,
            $repository->getFriendship(1, 2)
        );
        // level 3

        $expectedResponse[] = array('id' => 5,
            'first_name' => "user5 first name",
            'last_name' => "user5 last name"
        );

        $this->assertEquals($expectedResponse,
            $repository->getFriendship(1, 3)
        );

    }

    public function testGetFriendshipRequests()
    {
        $this->seedData();
        $redis = Redis::connection();
        $redis->sadd("user:1:requests", 3);
        $repository = new App\Repositories\FriendshipRepository();

        $this->assertEquals(array(array(
            'id' => 3,
            'first_name' => "user3 first name",
            'last_name' => "user3 last name")),
            $repository->getFriendshipRequests(1)
        );
    }
    public function testCreateOrAcceptFriendship()
    {
        $this->seedData();

        $repository = new App\Repositories\FriendshipRepository();
        $repository->createOrAcceptFriendship(3, 1);
        $this->assertEquals(array(array(
            'id' => 3,
            'first_name' => "user3 first name",
            'last_name' => "user3 last name")),
            $repository->getFriendshipRequests(1)
        );

        //should accept friendship
        $repository->createOrAcceptFriendship(1, 3);

        $this->assertContains(array(
            'id' => 3,
            'first_name' => "user3 first name",
            'last_name' => "user3 last name"),
            $repository->getFriendship(1)
        );

        $this->assertEquals(array(array(
            'id' => 1,
            'first_name' => "user1 first name",
            'last_name' => "user1 last name"),
            ),
            $repository->getFriendship(3)
        );

        $this->assertEquals(array(),
            $repository->getFriendshipRequests(1)
        );

    }

    public function testDeleteOrRejectFriendship()
    {
        $this->seedData();
        $redis = Redis::connection();
        $redis->sadd("user:1:requests", 3);
        $repository = new App\Repositories\FriendshipRepository();

        $repository->deleteOrRejectFriendship(3, 1);
        $this->assertEquals(array(),
            $repository->getFriendshipRequests(3)
        );

        //should remove friendship

        $repository->deleteOrRejectFriendship(2, 1);
        $this->assertEquals(array(),
            $repository->getFriendshipRequests(2)
        );
    }


}
