<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    protected function seedData()
    {
        // yes, it will reset existed redis data. i assume that we use separate redis server on testing env
        $redis = Redis::connection();
        $redis->eval("for _,k in ipairs(redis.call('keys', ARGV[1])) do redis.call('del', k) end", 0, "user:*");
        for ($i = 1; $i <= 5; $i++) {
            $redis->hMset("user:$i", array('id' => $i, 'first_name' => "user$i first name", 'last_name' => "user$i last name"));
        }

        $redis->sadd("user:1:friends", 2);
        $redis->sadd("user:2:friends", 1);
    }
}
