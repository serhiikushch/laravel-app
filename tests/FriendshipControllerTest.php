<?php

class FriendshipControllerTest extends TestCase {


   public function testGetUserFriendship()
   {
       $this->seedData();
       $response = $this->get('/friendship/1');
       $response->assertResponseOk();
   }

    public function testFriendshipNotFound()
    {
        $this->seedData();
        $response = $this->get('/friendship/10000000');
        $response->assertResponseStatus(404);
    }

    public function testCreateFriendship()
    {
        $this->seedData();
        $this->withoutMiddleware();
        $response = $this->post('/friendship/create', array('user_id' => 3, 'friend_id' => 1));
        $response->assertResponseOk();

        $response = $this->post('/friendship/create', array('user_id' => 3, 'friend_id' => 1));
        $response->assertResponseStatus(400);
    }

    public function testDeleteFriendship()
    {
        $this->seedData();
        $this->withoutMiddleware();
        $response = $this->post('/friendship/delete', array('user_id' => 1, 'friend_id' => 2));
        $response->assertResponseOk();
    }

}
