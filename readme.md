# Requirements
    redis server ~2.8.4 with default settings

# Installation and running
    cd /path/to/app
    composer install
    cp .env.example .env
    php artisan key:generate
    php artisan seed:friends {count_of_users}
    php artisan serve

## Usage
    GET /friendship/{user_id} - get user's friends. nesting level is 1 by default
    GET /friendship/{user_id}?level=N - get user's friends untill N level
    GET /friendship/{user_id}/requests - get incoming friendship requests
    POST /friendship/create?user_id={user_id}&friend_id={friend_id} send friendship request or accept incoming friendship request
    POST /friendship/delete?user_id={user_id}&friend_id={friend_id} reject incoming friendship request or remove user from friends

## Tests
    Note: tests will override seed data, because they use the same redis server, so run `php artisan seed:friends` after tests. (Lets assume we use separate redis server for test)
    cd /path/to/app
    phpunit tests/
